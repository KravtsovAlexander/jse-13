package ru.t1.kravtsov.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
