package ru.t1.kravtsov.tm.controller;

import ru.t1.kravtsov.tm.api.controller.IProjectController;
import ru.t1.kravtsov.tm.api.service.IProjectService;
import ru.t1.kravtsov.tm.api.service.IProjectTaskService;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void displayProjects() {
        System.out.println("[PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        int index = 1;
        for (final Project project : projects) {
            final String name = project.getName();
            final String description = project.getDescription();
            final String id = project.getId();
            final String status = Status.toName(project.getStatus());
            System.out.printf("%s. (%s) %s : %s : %s\n", index, id, name, description, status);
            index++;
        }
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        final List<Project> projects = projectService.findAll();
        projectTaskService.removeProjects(projects);
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer input = TerminalUtil.nextNumber();
        final Integer index = input - 1;
        final Project project = projectService.findOneByIndex(index);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void removeProjectsByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final List<Project> projectsForDeletion = new ArrayList<>();
        for (final Project project : projectService.findAll()) {
            if (project.getName() == null) continue;
            if (project.getName().equals(name)) {
                projectsForDeletion.add(project);
            }
        }
        projectTaskService.removeProjects(projectsForDeletion);
    }

    @Override
    public void displayProjectById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        displayProject(project);
    }

    @Override
    public void displayProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer input = TerminalUtil.nextNumber();
        final Integer index = input - 1;
        final Project project = projectService.findOneByIndex(index);
        displayProject(project);
    }

    private void displayProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer input = TerminalUtil.nextNumber();
        final Integer index = input - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusById(id, status);
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusByIndex(index, status);
    }

}
